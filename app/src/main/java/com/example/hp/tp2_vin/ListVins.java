package com.example.hp.tp2_vin;

import android.content.Intent;
import android.database.Cursor;
import android.icu.text.IDNA;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.util.ListUpdateCallback;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ListVins extends AppCompatActivity {

    ListView listV;
    WineDbHelper wineHelp;
    Cursor result;
    Intent myIntent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_vins);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        wineHelp = new WineDbHelper(this);

        result=wineHelp.fetchAllWines();
        System.out.println("iciii");

        listV=(ListView)findViewById(R.id.ListVin);
        listV.setVisibility(View.VISIBLE);
        SimpleCursorAdapter adapter = new SimpleCursorAdapter (this, android.R.layout.simple_list_item_2, result, new String[]{WineDbHelper.COLUMN_NAME, WineDbHelper.COLUMN_WINE_REGION}, new int[] {android.R.id.text1, android.R.id.text2});
        listV.setAdapter(adapter);
        System.out.println("adapterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr");
        listV.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            public void onItemClick(AdapterView<?> parent, View view, int position, long id){
                result.moveToPosition(position);
                Wine vin = WineDbHelper.cursorToWine(result);
                if (vin == null) {Log.d("~", "vin est null");}else{Log.d("~", "vin n'est pas null");}
                myIntent = new Intent(ListVins.this, InfoVins.class).putExtra("Wine", vin);
                myIntent.putExtra("type", InfoVins.UPDATE);
               ListVins.this.startActivity(myIntent);

            }
        });


        listV.setOnCreateContextMenuListener(this);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Wine wine = new Wine(" "," "," "," "," ");
                Intent myIntent = new Intent(ListVins.this, InfoVins.class);
                myIntent.putExtra("Wine", wine);
                myIntent.putExtra("type",InfoVins.ADD);
                ListVins.this.startActivity(myIntent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_list_vins, menu);
        return true;
    }

    @Override
    public boolean onContextItemSelected (MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item
                .getMenuInfo();
        String action = String.valueOf(item.getTitle());
        if(action.equals("Supprimer")){
            long position = info.id;
            result.moveToPosition((int) position-1);
            Wine wine = WineDbHelper.cursorToWine(result);
            if(wineHelp.deleteWine(wine)){
                Snackbar.make(info.targetView, wine.getTitle()+" Supprimé !", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                Log.i("supression","effectuer");
                // refresh
                wineHelp = new WineDbHelper(this);
                Cursor result = wineHelp.fetchAllWines();
                SimpleCursorAdapter adapter = new SimpleCursorAdapter (this, android.R.layout.simple_list_item_2, result, new String[]{WineDbHelper.COLUMN_NAME, WineDbHelper.COLUMN_WINE_REGION}, new int[] {android.R.id.text1, android.R.id.text2});
                listV.setAdapter(adapter);
            }
            else{
                Snackbar.make(info.targetView, "Suppression echoué", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                Log.i("supression","non effectuer");
            }

        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.add(0, v.getId(), 0, "Supprimer");
    }


}
