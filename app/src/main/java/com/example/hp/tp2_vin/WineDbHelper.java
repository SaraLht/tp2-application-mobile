package com.example.hp.tp2_vin;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by HP on 10/02/2020.
 */

public class WineDbHelper extends SQLiteOpenHelper{

    private static final String TAG = WineDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "wine.db";

    public static final String TABLE_NAME = "cellar";

    public static final String _ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_WINE_REGION = "region";
    public static final String COLUMN_LOC = "localization";
    public static final String COLUMN_CLIMATE = "climate";
    public static final String COLUMN_PLANTED_AREA = "publisher";
    SQLiteDatabase db;


    public WineDbHelper(Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
       // SQLiteDatabase SQLiteDatabase = this.getWritableDatabase();
        Log.i("WineDbHelper", "CA MARCHE");


    }

    // onCreate done
    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i("DataBase", "ON CREATE DEBUT");
        String CREATE_TABLE = "CREATE TABLE" +" "+ TABLE_NAME +" ("
                + _ID + " integer PRIMARY KEY AUTOINCREMENT," +COLUMN_NAME+ " TEXT,"
                + COLUMN_WINE_REGION +" TEXT," + COLUMN_LOC + " TEXT," + COLUMN_CLIMATE +
                " TEXT," + COLUMN_PLANTED_AREA + " TEXT" + ")";

        db.execSQL(CREATE_TABLE);
        Log.i("onCreate", "CA MARCHE");
        populate();


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.close();
        onCreate(db);
        Log.i("onUpgrade", "onUpgrade");
    }

    public boolean dup(Wine wine){
        SQLiteDatabase db = this.getReadableDatabase();
        String[] tableColumns = new String[] {
                "*",
        };
        String whereClause = COLUMN_NAME+" = ? AND "+COLUMN_WINE_REGION+" = ? AND _ID <> ?";
        String[] whereArgs = new String[] {
                wine.getTitle(),
                wine.getRegion(),
                wine.getId()+""
        };
        Cursor c = db.query(TABLE_NAME, tableColumns, whereClause, whereArgs, null, null, null);
               Log.i("dup","marche");
        return c.getCount() >0;
    }

    /**
     * Adds a new wine
     * @return  true if the wine was added to the table ; false otherwise (case when the pair (name, region) is
     * already in the data base
     */
    // addWine done
    public boolean addWine(Wine wine)  throws WineDuplicateException, WineEmptyException{
        System.out.println("ROOOOOOOOOOOOOWWWWWWWWWW");
        db = this.getWritableDatabase();
        if(dup(wine)){
            throw new WineDuplicateException();
        }
        if(wine.getTitle().trim().equals("")) throw new WineEmptyException();

        ContentValues values = new ContentValues();

        values.put(COLUMN_NAME,wine.getTitle().trim());
        values.put(COLUMN_WINE_REGION,wine.getRegion().trim());
        values.put(COLUMN_LOC,wine.getLocalization().trim());
        values.put(COLUMN_CLIMATE,wine.getClimate().trim());
        values.put(COLUMN_PLANTED_AREA,wine.getPlantedArea().trim());

        // Inserting Row
        long rowID = 0;
        // call db.insert()
        db.insert(TABLE_NAME, null,values);
        System.out.println("valuessss"+values);
       // db.close(); // Closing database connection
        Log.i("addWine", " CA MARCHE");

        return (rowID != -1);
    }

    /**
     * Updates the information of a wine inside the data base
     * @return the number of updated rows
     */
    // updateWines done
    public boolean  updateWine(Wine wine) throws WineDuplicateException, WineEmptyException{
         db = this.getWritableDatabase();
        if(dup(wine)){
            throw new WineDuplicateException();
        }
        if(wine.getTitle().trim().equals("")) throw new WineEmptyException();

        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_NAME, wine.getTitle().trim());
        contentValues.put(COLUMN_WINE_REGION, wine.getRegion().trim());
        contentValues.put(COLUMN_CLIMATE, wine.getClimate().trim());
        contentValues.put(COLUMN_LOC, wine.getLocalization().trim());
        contentValues.put(COLUMN_PLANTED_AREA, wine.getPlantedArea().trim());
        int res = db.update(TABLE_NAME, contentValues, "_ID = " + wine.getId(), null);
        db.close();
        Log.i("updateWine", " CA MARCHE");
        return (res > 0);

    }

    /**
     * Returns a cursor on all the wines of the library
     */
    // fetchAllWines done
    public Cursor fetchAllWines() {
        System.out.println("Fetching");
        db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM "+TABLE_NAME,null);
        Log.i("fetchAllWines", "CA MARCHE");
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;


    }

    // deleteWine done : delete a column _id
    public Boolean deleteWine(Wine wine) {
        db = this.getWritableDatabase();
        int res = db.delete(TABLE_NAME, "_ID = " + wine.getId(), null);
        db.close();
        Log.i("deleteWine", "CA MARCHE");
        return res>0;
    }

    public void populate() {
        Log.d(TAG, "call populate()");
        try {
            addWine(new Wine("Châteauneuf-du-pape", "vallée du Rhône ", "Vaucluse", "méditerranéen", "3200"));
            addWine(new Wine("Arbois", "Jura", "Jura", "continental et montagnard", "812"));
            addWine(new Wine("Beaumes-de-Venise", "vallée du Rhône", "Vaucluse", "méditerranéen", "503"));
            addWine(new Wine("Begerac", "Sud-ouest", "Dordogne", "océanique dégradé", "6934"));
            addWine(new Wine("Côte-de-Brouilly", "Beaujolais", "Rhône", "ocÃ©anique et continental", "320"));
            addWine(new Wine("Muscadet", "vallée de la Loire", "Loire-Atlantique", "océanique", "9000"));
            addWine(new Wine("Bandol", "Provence", "Var", "méditerranéen", "1500"));
            addWine(new Wine("Vouvray", "Indre-et-Loire", "Indre-et-Loire", "océanique dégradé", "2000"));
            addWine(new Wine("Ayze", "Savoie", "Haute-Savoie", "continental et montagnard", "20"));
            addWine(new Wine("Meursault", "Bourgogne", "Côte-d'Or", "océanique et continental", "395"));
            addWine(new Wine("Ventoux", "Vallée du Rhône", "Vaucluse", "méditerranéen", "7450"));

        } catch (WineDuplicateException e) {

        } catch (WineEmptyException e) {

        }
        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM " + TABLE_NAME, null);
        Log.d(TAG, "nb of rows=" + numRows);
        Log.i("populate", "CA MARCHE");
        db.close();

    }



    public static Wine cursorToWine(Cursor c) {
        Wine wine = new Wine(c.getLong(0), c.getString(1), c.getString(2),
                c.getString(3), c.getString(4), c.getString(5));
        Log.i("cursorToWine","CA MARCHE");
        return wine;

    }

}
class WineEmptyException extends  Exception {

}
class WineDuplicateException extends  Exception {

}