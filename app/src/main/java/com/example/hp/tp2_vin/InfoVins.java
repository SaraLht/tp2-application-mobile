package com.example.hp.tp2_vin;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class InfoVins extends AppCompatActivity {
    public Wine wine = null;
    public static final String ADD = "add";
    public static final String UPDATE = "update";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_vins);
        wine = (Wine) getIntent().getParcelableExtra("Wine");
        final String type = getIntent().getStringExtra("type");

        final EditText name = findViewById(R.id.wineName);
        if (name == null) Log.d("~", "name is null");
        name.setText(wine.getTitle());

        final EditText region = findViewById(R.id.editWineRegion);
        if (region == null) Log.d("~", "region is null");
        region.setText(wine.getRegion());

        final EditText loc = findViewById(R.id.editLoc);
        if (loc == null) Log.d("~", "loc is null");
        loc.setText(wine.getLocalization());

        final EditText plantedArea = findViewById(R.id.editPlantedArea);
        if (plantedArea == null) Log.d("~", "plantedArea is null");
        plantedArea.setText(wine.getPlantedArea());

        final EditText climate = findViewById(R.id.editClimate);
        if (climate == null) Log.d("~", "climate is null");

        climate.setText(wine.getClimate());


        final WineDbHelper wineDbHelper = new WineDbHelper(this);
        Button save = findViewById(R.id.button);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    wine.setClimate(climate.getText().toString());
                    wine.setRegion(region.getText().toString());
                    wine.setPlantedArea(plantedArea.getText().toString());
                    wine.setLocalization(loc.getText().toString());
                    wine.setTitle(name.getText().toString());
                    boolean res = false;
                    if (type.equals(UPDATE))
                        res = wineDbHelper.updateWine(wine);
                    else if (type.equals(ADD)) {
                        res = wineDbHelper.addWine(wine);
                    }
                    if (res==true) { // a revoir
                        Snackbar.make(v, "Vin Enregistré", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    } else {
                        Snackbar.make(v, "Erreur !", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                }
                  catch(WineDuplicateException e){

                      AlertDialog.Builder b=new AlertDialog.Builder(InfoVins.this);
                      b.setTitle("Sauvegarde impossible");
                      b.setMessage("Le vin portant le meme nom éxiste déja dans la base de données")
                              .show();

                    }
                catch(WineEmptyException e){

                    AlertDialog.Builder b=new AlertDialog.Builder(InfoVins.this);
                    b.setTitle("Ajout impossible");
                    b.setMessage("Le nom du vin ne doit pas etre vide")
                            .show();


                }
                }
        });
    }
}

